primesieve (12.7+ds-1) unstable; urgency=medium

  * New upstream minor version.
  * Debianization:
    - d/{copyright,adhoc/{c,cpp}/Makefile, copyright year-tuples, refrech;
    - d/control:
      - Standards-Version, bump to 4.7.2 (no change).

 -- Jerome Benoit <calculus@rezozer.net>  Sun, 09 Mar 2025 14:57:31 +0000

primesieve (12.6+ds-1) unstable; urgency=medium

  * New upstream minor version.

 -- Jerome Benoit <calculus@rezozer.net>  Sun, 01 Dec 2024 16:32:10 +0000

primesieve (12.5+ds-1) unstable; urgency=medium

  * New upstream minor version.

 -- Jerome Benoit <calculus@rezozer.net>  Sun, 10 Nov 2024 02:04:41 +0000

primesieve (12.4+ds-1) unstable; urgency=medium

  * New upstream minor version.

 -- Jerome Benoit <calculus@rezozer.net>  Sun, 01 Sep 2024 12:55:50 +0000

primesieve (12.3+ds-1) unstable; urgency=medium

  * New upstream minor version.
  * Debianization:
    - d/t/make-check-dev, refresh;
    - d/control:
      - Standards-Version, bump to 4.7.0 (no change).

 -- Jerome Benoit <calculus@rezozer.net>  Sun, 21 Apr 2024 10:52:21 +0000

primesieve (12.1+ds-1) unstable; urgency=medium

  * New upstream minor version.
  * Debianization:
    - d/copyright:
      - Files-Excluded list, refresh.

 -- Jerome Benoit <calculus@rezozer.net>  Thu, 14 Mar 2024 09:33:37 +0000

primesieve (12.0+ds-2) unstable; urgency=medium

  * Debianization:
    - d/t/control:
      - Depends list, when applicable, rename pkg-config pkgconf.

 -- Jerome Benoit <calculus@rezozer.net>  Sat, 02 Mar 2024 19:30:01 +0000

primesieve (12.0+ds-1) unstable; urgency=medium

  * New upstream major version.
  * Debianization:
    - SONAME, bump to 12;
    - d/rules:
      - override_dh_installdocs target, SONAME machinery, harden;
    - d/t/make-check-dev, refresh;
    - d/control:
      - dev Suggests list, rename pkg-config pkgconf.

 -- Jerome Benoit <calculus@rezozer.net>  Sat, 24 Feb 2024 10:00:50 +0000

primesieve (11.2+ds-2) unstable; urgency=medium

  * Fail-to-Auto-Test fix release, it appeared that some headers
    were reorganized.
  * Debianization:
    - d/t/make-check-dev, refresh.

 -- Jerome Benoit <calculus@rezozer.net>  Fri, 26 Jan 2024 17:33:47 +0000

primesieve (11.2+ds-1) unstable; urgency=medium

  * New upstream minor release.
  * Debianization:
    - d/copyright:
      - copyright year-tuples, update.

 -- Jerome Benoit <calculus@rezozer.net>  Thu, 25 Jan 2024 16:28:26 +0000

primesieve (11.1+ds-1) unstable; urgency=medium

  * New upstream minor release.
  * Debianization:
    - d/copyright:
      - copyright year-tuples, update;
    - d/t/control:
      - dev-autogtests  allow stderr outputs (Closes: #1027814).

 -- Jerome Benoit <calculus@rezozer.net>  Sun, 16 Jul 2023 13:23:43 +0000

primesieve (11.0+ds-2) unstable; urgency=medium

  * Team upload
  * Raising Standards version to 4.6.2 (no change)

 -- Pierre Gruet <pgt@debian.org>  Wed, 21 Dec 2022 18:10:23 +0100

primesieve (11.0+ds-1) unstable; urgency=medium

  * New upstream major release, new (C++) ABI (minor changes).
  * Debianization:
    - various minor janitorial changes.

 -- Jerome Benoit <calculus@rezozer.net>  Sat, 10 Dec 2022 18:32:19 +0000

primesieve (8.0+ds-3) unstable; urgency=medium

  * Debianization:
    - d/watch, update;
    - d/s/lintian-overrides, update.

 -- Jerome Benoit <calculus@rezozer.net>  Sun, 27 Nov 2022 15:20:54 +0000

primesieve (8.0+ds-2) unstable; urgency=medium

  * SBFR, sanitize d/control (Closes: #1014784).

 -- Jerome Benoit <calculus@rezozer.net>  Mon, 11 Jul 2022 20:38:41 +0000

primesieve (8.0+ds-1) unstable; urgency=medium

  * New upstream major release, new ABI (minor changes).
  * Debianization:
    - SONAME, bump to 10;
    - d/control:
      - Standards-Version, bump to 4.6.1 (no change);
    - d/tests:
      - d/t/make-check-dev, update;
    - d/rules, harden.

 -- Jerome Benoit <calculus@rezozer.net>  Sat, 09 Jul 2022 10:45:42 +0000

primesieve (7.9+ds-1) unstable; urgency=medium

  * New upstream minor release.

 -- Jerome Benoit <calculus@rezozer.net>  Sun, 08 May 2022 12:04:41 +0000

primesieve (7.8+ds-1) unstable; urgency=medium

  * New upstream minor release.
  * Debianization:
    - d/control, migration to Debian Math Team group;
    - d/copyright:
      - copyright year tuples, update;
    - d/adhoc/examples/*/Makefile, refresh;
    - d/t/make-check-bin, update.

 -- Jerome Benoit <calculus@rezozer.net>  Sat, 05 Feb 2022 11:20:07 +0000

primesieve (7.7+ds-1) unstable; urgency=medium

  * New upstream minor release.
  * Debianization:
    - d/control:
      - debhelper, bump to 13;
    - d/upstream/metadata, introduce.

 -- Jerome Benoit <calculus@rezozer.net>  Sat, 11 Dec 2021 14:51:52 +0000

primesieve (7.6+ds-4) unstable; urgency=medium

  * Debianization:
    - d/libprimesieve-dev.install, now install cmake material (thanks to
      Julien Schueller for noticing);
    - d/control:
      - Standards-Version, bump to 4.6.0 (no change).

 -- Jerome Benoit <calculus@rezozer.net>  Mon, 11 Oct 2021 11:01:23 +0000

primesieve (7.6+ds-3) unstable; urgency=medium

  * Upload to unstable.

 -- Jerome Benoit <calculus@rezozer.net>  Sun, 29 Aug 2021 15:33:56 +0000

primesieve (7.6+ds-2) experimental; urgency=medium

  * Debianization
    - d/watch, refresh.

 -- Jerome Benoit <calculus@rezozer.net>  Sun, 20 Jun 2021 11:49:35 +0000

primesieve (7.6+ds-1) unstable; urgency=medium

  * New upstream minor release.
  * Debianization:
    - d/copyright:
      - Files-Excluded list, refresh;
      - copyright year tuple, update;
      - gui Files section, discard;
      - Debian License, migrate to BSD-2-clause;
    - d/control:
      - Rules-Requires-Root, introduce and set to no;
      - Standards-Version, bump to 4.5.1 (no change);
      - multiarch medata, fix;
    - d/primesieve-doc.docs, refresh.

 -- Jerome Benoit <calculus@rezozer.net>  Tue, 19 Jan 2021 19:08:44 +0000

primesieve (7.5+ds-4) unstable; urgency=medium

  * Team upload
  * Allow stderr output in autopkgtests for GCC warnings on armhf

 -- Graham Inggs <ginggs@debian.org>  Sun, 04 Oct 2020 12:44:03 +0000

primesieve (7.5+ds-3) unstable; urgency=medium

  * Debianization:
    - debian/control:
      - Build-Depends, add xmlto.

 -- Jerome Benoit <calculus@rezozer.net>  Mon, 30 Dec 2019 20:22:15 +0000

primesieve (7.5+ds-2) unstable; urgency=medium

  * Debianization:
    - debian/control:
      - Build-Depends, replace asciidoc with asciidoc-base.

 -- Jerome Benoit <calculus@rezozer.net>  Mon, 30 Dec 2019 16:45:54 +0000

primesieve (7.5+ds-1) unstable; urgency=medium

  * New upstream minor release.
  * Debianization:
    - debian/copyright:
      - Source field, migrate to github upstream page;
    - debian/control:
      - Standards Version, bump to 4.4.1 (no change);
      - Build-Depends, replace help2man with asciidoc;
      - Homepage field, migrate to github upstream page;
    - debian/tests/*:
      - d/t/{make-check-{bin,dev},build-examples}, now use AUTOPKGTEST_TMP;
      - d/t/make-check-dev, refresh;
    - debian/rules:
      - override_dh_auto_configure target, add BUILD_MANPAGE=ON option;
    - debian/primesieve-doc.docs, add doc/ALGORITHMS.md

 -- Jerome Benoit <calculus@rezozer.net>  Mon, 30 Dec 2019 13:58:13 +0000

primesieve (7.4+ds-2) unstable; urgency=medium

  * Upload to unstable.
  * Debianization:
    - debian/control:
      - Standards Version, bump to 4.4.0 (no change).

 -- Jerome Benoit <calculus@rezozer.net>  Fri, 16 Aug 2019 05:53:41 +0000

primesieve (7.4+ds-1) experimental; urgency=medium

  * New upstream minor release.
  * Debianization:
    - debian/tests/*:
      - d/t/control, add self-test check;
    - debian/control:
      - Build-Depends, migrate to debhelper-compat (=12);
    - debian/compat, discard.

 -- Jerome Benoit <calculus@rezozer.net>  Fri, 26 Apr 2019 06:02:10 +0000

primesieve (7.3+ds-1) unstable; urgency=medium

  * New upstream minor release.
  * Debianization:
    - debian/{copyright,adhoc/c{,pp}/Makefile}:
      - Copyright year tuple, update;
    - debian/control:
      - Standards Version, bump to 4.3.0 (no change).

 -- Jerome Benoit <calculus@rezozer.net>  Thu, 17 Jan 2019 14:39:34 +0000

primesieve (7.2+ds-1) unstable; urgency=medium

  * New upstream minor release.

 -- Jerome Benoit <calculus@rezozer.net>  Wed, 31 Oct 2018 03:11:41 +0000

primesieve (7.1+ds-1) unstable; urgency=medium

  * New upstream minor release.
  * Debianization:
    - debian/control:
      - Standards Version, bump to 4.2.1 (no change);
    - debian/tests/*:
      - d/t/make-check-dev , update;
    - debian/clean , refresh.

 -- Jerome Benoit <calculus@rezozer.net>  Sat, 22 Sep 2018 14:56:54 +0000

primesieve (7.0+ds-3) unstable; urgency=medium

  * RC bug fix release (Closes: #901128), bump library so-suffix.
  * autopkgtests bug fix post-release (Closes: #901098).
  * Debianization:
    - debian/{control,rules}:
      - library so-suffix, bump to 9.

 -- Jerome Benoit <calculus@rezozer.net>  Sat, 09 Jun 2018 07:08:21 +0000

primesieve (7.0+ds-2) unstable; urgency=medium

  * autopkgtests bug fix release.
  * Debianization:
    - debian/tests/*:
      - d/t/make-check-dev , refresh.

 -- Jerome Benoit <calculus@rezozer.net>  Sat, 09 Jun 2018 06:06:25 +0000

primesieve (7.0+ds-1) unstable; urgency=medium

  * New upstream major release.
  * Debianization:
   - debian/copyright:
      - Format field, secure URI;
    - debian/control:
      - Standards Version, bump to 4.1.4 (no change);
      - Homepage field, secure URI;
      - Vcs-* fields, migration to Salsa;
    - debian/source:
      - d/s/lintian-overrides , update;
    - debian/rules:
      - get-orig-source target, discard;
      - README.implementation.md target, refresh (cascade change);
    - debian/patches:
      - d/p/upstream-fix-cmake_machinery-libatomic.patch , integrated.

 -- Jerome Benoit <calculus@rezozer.net>  Fri, 08 Jun 2018 09:28:07 +0000

primesieve (6.3+ds-2) unstable; urgency=medium

  * Debianization:
    - debian/tests/*:
      - d/t/make-check[-bin], revisit (Closes: #884349),
        	thanks to upstream maintainer;
      - d/t/make-check-dev, introduce;
    - debian/copyright, refresh;
    - debian/control:
      - debhelper, bump to 11: (revisit d/primesieve-doc.docs);
        - d/primesieve-doc.docs, revisit;
        - d/t/build-examples, revisit;
        - d/adhoc/examples/c*/Makefile, revisit;
      - Standards Version, bump to 4.1.3 (no change);
    - refresh.

 -- Jerome Benoit <calculus@rezozer.net>  Sat, 06 Jan 2018 12:50:07 +0000

primesieve (6.3+ds-1) unstable; urgency=medium

  * New upstream minor release (Closes: #881453, #881678).
  * Debianization:
    - debian/rules, simplify;
    - debian/patches/*:
      - d/p/upstream-fix-cmake_machinery-libatomic.patch, introduce.

 -- Jerome Benoit <calculus@rezozer.net>  Fri, 24 Nov 2017 13:51:00 +0000

primesieve (6.2+ds-1) unstable; urgency=medium

  * New upstream major release.
  * Debianization:
    - debian/watch, correct;
    - debian/copyright, update;
    - debian/control:
      - debhelper, bump to 10;
      - Standards Version, bump to 4.1.1 (no change);
      - upgrade SO version to version 8;
    - debian/rules:
      - migrate from autotools to cmake machinery (see below);
      - debhelper, bump to 10;
      - refresh;
    - debian/patches/*:
      - d/p/upstream-autotoolization-manpages.patch, discard;
      - d/p/debianization*.patch, discard;
      - d/p/upstream-cmake-install-*.patch, introduce and submit;
    - debian/libprimesieve7.*, bump to version 8;
    - debian/libprimesieve7-dev*.*, bump to versionless;
    - migration from autotools to cmake machinery;
    - refresh.

 -- Jerome Benoit <calculus@rezozer.net>  Sat, 11 Nov 2017 05:56:10 +0000

primesieve (5.7.2+ds-2) unstable; urgency=medium

  * RC bug fix release (Closes: #860664), neutralize the heavy test that
    challenges resources.

 -- Jerome Benoit <calculus@rezozer.net>  Sat, 22 Apr 2017 14:59:07 +0000

primesieve (5.7.2+ds-1) unstable; urgency=medium

  * New upstream release.
  * Debianization:
    - debian/copyright, update;
    - debian/rules, refresh;
    - debian/adhoc/Makefile, ad hoc manpage machinery, discard;
    - debian/patches/*:
      - d/p/upstream-source-CPP2help2man.patch, discard;
      - d/p/upstream-autotoolization-manpages.patch, introduce and submit;
      - d/p/debianization, refresh;
    - refresh.

 -- Jerome Benoit <calculus@rezozer.net>  Tue, 15 Nov 2016 02:08:05 +0000

primesieve (5.7.1+ds-1) unstable; urgency=medium

  * New upstream release.
  * Debianization:
    - debian/copyright, refresh;
    - debian/patches/:
      - d/p/upstream-source-CPP2help2man.patch, reintroduce.

 -- Jerome Benoit <calculus@rezozer.net>  Thu, 18 Aug 2016 01:55:09 +0000

primesieve (5.7.0+ds-2) unstable; urgency=medium

  * RC bug fix release (Closes: #833870), harden libdev transition.

 -- Jerome Benoit <calculus@rezozer.net>  Wed, 10 Aug 2016 00:58:23 +0000

primesieve (5.7.0+ds-1) unstable; urgency=medium

  * New upstream release.
  * Debianization:
    - debian/watch:
      - bump to 4;
      - repack option, add;
    - debian/control:
      - upgrade SO version to version 7;
      - Suggests/Enhances fields, revisit;
    - debian/libprimesieve6*, bump to version 7;
    - debian/rules, refresh;
    - debian/adhoc/:
      - d/a/cpp/Makefile, harden;
    - refresh.

 -- Jerome Benoit <calculus@rezozer.net>  Thu, 04 Aug 2016 15:00:58 +0000

primesieve (5.6.0+ds-2) unstable; urgency=medium

  * Debianization:
    - debian/copyright:
      - refresh;
    - debian/control:
      - discard the debug symbols package libbliss1d-dbg;
      - Standards Version, bump to 3.9.8 (no change);
      - Vcs-Git header, secure;
    - debian/rules:
      - discard the debug symbols material (see above);
      - dpkg-buildflags, add hardening=+all;
    - reproducible build, attempt.

 -- Jerome Benoit <calculus@rezozer.net>  Sat, 14 May 2016 14:34:00 +0000

primesieve (5.6.0+ds-1) unstable; urgency=medium

  * New upstream release.
  * Debianization:
    - debian/copyright:
      - Files-Excluded field, refine;
    - debian/control, upgrade SO version to version 6;
    - debian/libprimesieve5*, idem;
    - debian/rules, idem;
    - debian/clean, refresh;
    - debian/adhoc/Makefile, idem.

 -- Jerome Benoit <calculus@rezozer.net>  Sat, 19 Dec 2015 19:39:29 +0000

primesieve (5.5.0+ds-1) unstable; urgency=medium

  * New upstream release.
  * Debianization:
    - debian/rules, refresh;
    - debian/adhoc/Makefile, update.

 -- Jerome Benoit <calculus@rezozer.net>  Mon, 09 Nov 2015 22:26:13 +0000

primesieve (5.5.0~rc1+ds-1) unstable; urgency=medium

  * Initial release. (Closes: #802513)

 -- Jerome Benoit <calculus@rezozer.net>  Tue, 27 Oct 2015 21:00:34 +0000
